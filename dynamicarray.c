/*-------------------------------------------- information --------------------------------------------------------------------------*/
		//written by: github/return5
		//release under GPL 2.0
		//example of a dynamic array in C using structs and unions to allow making dynamic arrays from multiple data types.
		//i make no claim this is the best or most effecient way to do dynamic arrays in C, just an example i made. 

/*-------------------------------------------- headers ------------------------------------------------------------------------------*/
#include <stdio.h>  //printf and snprintf
#include <stdlib.h> //mallloc adn realloc
#include <string.h>  //strlen

/*-------------------------------------------- typedefs -----------------------------------------------------------------------------*/
typedef union data { //union which is used to hold elements in array. can be expanded to include other types.
	int i;
	double d;
	char *c;
}data;

//struct to hold array and relevent information
typedef struct dynamicArray {
	int length,index; 		  //length of array, index of array.
	size_t size_element; 	 //size of each array member
	data *arr; 				//pointer to union. used as array
	void (*addToArray)(struct dynamicArray *dynarr,const void* value);  //function pointer for adding elements.
	char *(*toString)(const struct dynamicArray *const dynarr, const int i);  //function pointer to toString function
}dynamicArray;

/*------------------------------------------- function prototypes ------------------------------------------------------------------*/
char *toStringCharPointer(const dynamicArray *dynarr, const int i);
char *toStringInt(const dynamicArray *const dynarr,const int i);
char *toStringDouble(const dynamicArray *const dynarr, const int i);
void makeDynamicIntArray(dynamicArray *const dynarr); 
void makeDynamicDoubleArray(dynamicArray *const dynarr); 
void makeDynamicCharPointerArray(dynamicArray *const dynarr); 
void fillCharArray(dynamicArray *const dynarr); 
void fillDoubleArray(dynamicArray *const dynarr); 
void fillIntArray(dynamicArray *const dynarr); 
void printElements(const dynamicArray *const dynarr); 
void addToIntArray(dynamicArray *dynarr, const void *value); 
void addToDoubleArray(dynamicArray *dynarr, const void *value); 
void addToCharPointerArray(dynamicArray *dynarr, const void *str); 
void checkSize(dynamicArray *dynarr); 

/*-------------------------------------------- functions ----------------------------------------------------------------------------*/

//checks current size of array.
void checkSize(dynamicArray *dynarr) {
	//if array is full
	if(dynarr->index == dynarr->length - 1) {
		dynarr->length *= 2;  //double length of array
		data *temp = realloc(dynarr->arr,dynarr->length * dynarr->size_element);  //allocate new space for expanded array
		if(temp == NULL) { //check if realloc failed, if so try again
			temp = realloc(dynarr->arr,dynarr->length * dynarr->size_element);
		}
		dynarr->arr = temp;  //set old array to new array
		printf("realloc \n"); //print information saying it was created.  can be removed in a real application of dynamic arrays
	}
}

//adds elements to char pointer array
void addToCharPointerArray(dynamicArray *dynarr, const void *str) {
	checkSize(dynarr);
	const size_t len_str = strlen(str) + 1;  //get len of str. add 1 for null terminator
	dynarr->arr[dynarr->index].c = malloc(len_str); //alocate space for char pointer in array
	snprintf(dynarr->arr[dynarr->index].c,len_str,(char *)str); //copy str into char pointer array;
	dynarr->index++;  //increment index
}

//add elements to double array
void addToDoubleArray(dynamicArray *dynarr, const void *value) {
	checkSize(dynarr);
	dynarr->arr[dynarr->index++].d = *((double*)value); //cast void pointer to double pointer then assign contents of it to double array
}

//adds elements to int array
void addToIntArray(dynamicArray *dynarr, const void *value) {
	checkSize(dynarr);
	dynarr->arr[dynarr->index++].i = *((int*)value);  //cast void pointer to int pointer, then assign contents of it to int array
}

//returns copy of string in char pointer array at index i
char *toStringCharPointer(const dynamicArray *dynarr, const int i){
	const size_t str_len = strlen(dynarr->arr[i].c) + 1;
	char *c = malloc(str_len);
	snprintf(c,str_len,dynarr->arr[i].c);
	return c;
}

//returns string representation of element in int array at index i
char *toStringInt(const dynamicArray *const dynarr,const int i){
	char *c = malloc(5);
	snprintf(c,5,"%d",dynarr->arr[i].i);
	return c;
}

//returns string representation of element in double array at index i
char *toStringDouble(const dynamicArray *const dynarr, const int i) {
	char *c = malloc(5);
	snprintf(c,5,"%f",dynarr->arr[i].d);
	return c;
}

//prints all elements of array
void printElements(const dynamicArray *const dynarr) {
	for(int i = 0; i < dynarr->index; i++) {
		char *str = dynarr->toString(dynarr,i);
		printf("element %d is %s\n",i,str);
		free(str); //free copy of string now that it isnt needed
	}
}
//fills array with ints numbers form 0 to 19
void fillIntArray(dynamicArray *const dynarr) {
	printf("filling int array\n");
	for(int i = 0; i < 20; i++) {
		printf("adding %d\n",i);
		dynarr->addToArray(dynarr,&i);
	}
}

//fills double array with floating point numbers from 0.0 to 1.0 
void fillDoubleArray(dynamicArray *const dynarr) {
	printf("filling double array\n");
	for(double d = 0; d < 1.0; d += .1 ){
		printf("adding double %f\n",d);
		dynarr->addToArray(dynarr,&d);
	}
}
//fills char pointer array with char pointers
void fillCharArray(dynamicArray *const dynarr) {
	printf("filling char pointer array\n");
	dynarr->addToArray(dynarr,"hello,");
	dynarr->addToArray(dynarr,"world!");
	dynarr->addToArray(dynarr,"how ");
	dynarr->addToArray(dynarr,"are ");
	dynarr->addToArray(dynarr,"you?");
	dynarr->addToArray(dynarr,"I ");
	dynarr->addToArray(dynarr,"am ");
	dynarr->addToArray(dynarr,"fine,");
	dynarr->addToArray(dynarr,"thank ");
	dynarr->addToArray(dynarr,"you.");
}


//takes in a pointer to a dynamicArray struct and makes a dynamic char pointer array
void makeDynamicCharPointerArray(dynamicArray *const dynarr) {
	dynarr->length = 5; //initial length of array
	dynarr->index = 0; //initial index
	dynarr->size_element = sizeof(data);   //set size for the type of element which arr will hold
	dynarr->arr = malloc(dynarr->size_element * dynarr->length); // create an array to hold length number of char pointers.
	dynarr->addToArray = addToCharPointerArray; //set addToArray function pointer to addToCharPointerArray function
	dynarr->toString = toStringCharPointer; //set the toString function pointer to toStringCharPointer function
}

//takes in a pointer to struct and makes a dynamic double array from it
void makeDynamicDoubleArray(dynamicArray *const dynarr) {
	dynarr->length = 5; //initial length of array
	dynarr->index = 0; //initial index
	dynarr->size_element = sizeof(data);   //set size for the type of element which arr will hold
	dynarr->arr = malloc(dynarr->size_element * dynarr->length); // create an array to hold length number of doubles.
	dynarr->addToArray = addToDoubleArray; //set addToArrayfunction pointer to addToDoubleArray function
	dynarr->toString = toStringDouble;  //set toString function poiinter to toStringDouble function
}
//takes in a pointer to struct and makes a dynamic int array from it
void makeDynamicIntArray(dynamicArray *const dynarr) {
	dynarr->length = 5; //initial length of array
	dynarr->index = 0; //initial index
	dynarr->size_element = sizeof(data);   //set size for the type of element which arr will hold
	dynarr->arr = malloc(dynarr->size_element * dynarr->length); // create an array to hold length number of ints.
	dynarr->addToArray = addToIntArray; //set addToArray function pointer to addToIntArray function
	dynarr->toString = toStringInt;   //set toString function pointer to ToStringInt function
}

int main(void) {
	dynamicArray dynarr_int;   //dynamicArray struct
	dynamicArray dynarr_double;  //dynamic double array
	dynamicArray dynarr_char;  //char* dynamic array
	makeDynamicIntArray(&dynarr_int); 
	makeDynamicDoubleArray(&dynarr_double);  
	makeDynamicCharPointerArray(&dynarr_char);
	fillIntArray(&dynarr_int);
	fillDoubleArray(&dynarr_double);
	fillCharArray(&dynarr_char);
	printf("printing int array\n");
	printElements(&dynarr_int);
	printf("printing double array\n");
	printElements(&dynarr_double);
	printf("printing char pointer array\n");
	printElements(&dynarr_char);
	return 0;
}
